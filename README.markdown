Automate some of the data request message generation.

# How to
1. Install hyde
2. Put request information in ./content
3. Run `hyde gen`
4. Requests appear in ./deploy
